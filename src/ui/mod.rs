#[cfg(feature = "gtk_3_10")]
pub mod ui {
    use gtk;
    use gtk::prelude::*;
    use gtk::{Builder, Window, ListStore, Button, TreeIter, TreePath, ComboBox};
    use rand;
    use rand::distributions::{Range};

    pub fn main() {
        if gtk::init().is_err() {
            println!("Failed to initialize GTK.");
            return;
        }

        let glade_src = include_str!("ui.glade");
        let builder = Builder::new_from_string(glade_src);

        let main_window: Window = builder.get_object("main_window").unwrap();
        let tree_view_list_store: ListStore = builder.get_object("tree_view_list_store").unwrap();
        let start_button: Button = builder.get_object("start_button").unwrap();
        let node_count: ComboBox = builder.get_object("node_count").unwrap();

        //        start_button.connect_clicked(move |_| {
        //            let iter: TreeIter = match tree_view_list_store.get_iter_first() {
        //                None => panic!("TreeView doesn't contain rows"),
        //                Some(i) => i
        //            };
        //
        //            while tree_view_list_store.iter_next(&iter) {
        //                tree_view_list_store.set(&iter, &[1], &[&"100"]);
        //            }
        //        });

        node_count.connect_changed(move |combo_box| {
            let id = combo_box.get_active() as u32;

            let between = Range::new(0, 1000);
            let mut rng = rand::thread_rng();

            let iter: TreeIter = match tree_view_list_store.get_iter_first() {
                None => panic!("TreeView doesn't contain rows"),
                Some(i) => i
            };

            //            let column_values = vec![&""; 16];
            //            let column_numbers = vec![];
            //            for i in 1..17 {
            //                column_numbers.push(i);
            //            }
            //
            //            tree_view_list_store.set(&iter, &column_numbers[..], &column_values[..]);
            //
            //            let cur_v = vec![];
            //            let cur_n = vec![];
            //
            //            for i in 1u32..id {
            //                cur_n.push(i);
            //                cur_v.push(&"123");
            //            }
            //
            //            while tree_view_list_store.iter_next(&iter) {
            //                tree_view_list_store.set(&iter, &column_numbers[..], &column_values[..]);
            //                tree_view_list_store.set(&iter, &cur_n[..], &cur_v[..]);
            //            }
            for i in 1u32..id {
                for j in 1u32..id {
                    tree_view_list_store.set(&iter, &[j], &[&j.to_string()]);
                }
                tree_view_list_store.iter_next(&iter);
            }

            println!("{}", id);
        });

        main_window.show_all();
        gtk::main();

        //    let window = gtk::Window::new(gtk::WindowType::Toplevel);
        //
        //    window.set_title("First GTK+ Program");
        //    window.set_border_width(10);
        //    window.set_position(gtk::WindowPosition::Center);
        //    window.set_default_size(350, 70);
        //
        //    window.connect_delete_event(|_, _| {
        //        gtk::main_quit();
        //        Inhibit(false)
        //    });
        //
        //    //let button = gtk::Button::new_with_label("Click me!");
        //
        //    let tree_view = gtk::TreeView::new();
        //    let tree_view_column1 = gtk::TreeViewColumn::new();
        //    tree_view_column1.set_title("Hello");
        //
        //    let tree_view_column2 = gtk::TreeViewColumn::new();
        //    tree_view_column2.set_title("test");
        //
        //    tree_view.append_column(&tree_view_column1);
        //    tree_view.append_column(&tree_view_column2);
        //
        //    //window.add(&button);
        //    window.add(&tree_view);
        //
        //
        //    window.show_all();
        //    gtk::main();
    }
}