#![crate_type = "bin"]

extern crate gtk;
extern crate rand;

mod ui;

#[cfg(feature = "gtk_3_10")]
fn main() {
    ui::ui::main();
}